﻿using KS.AI.FSM;
using UnityEngine;

namespace StdAssetCharVisualisation.AI
{
    public class Idle : State
    {
        public FSMFollowWaypoints LocalFSM { get; set; }

        private float timer;
        
        public Idle(FiniteStateMachine fsm) : base(fsm)
        {
            LocalFSM = (FSMFollowWaypoints)fsm;
        }

        public override void Enter()
        {
            float idleTime = Random.Range(LocalFSM.aiSettings.minIdleTime, LocalFSM.aiSettings.maxIdleTime);
            timer = idleTime;
            base.Enter();
        }

        public override void Update()
        {
            timer -= Time.deltaTime;
            if (timer < 0)
            {
                this.FSM.NextState = new FollowWaypoint(this.FSM);
                this.StateStage = StateEvent.EXIT;
            }
        }
        
        
    }
}
