﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class cannon : MonoBehaviour
{
    public float bulletSpeed = 10;
    public Rigidbody bullet;
    private float spawnTime;
    public float maxTime = 4;
    public float minTime = 1;
    private float time;
    Rigidbody bulletClone;

    void Start()
    {
        bulletClone = GetComponent<Rigidbody>();
        SetRandomTime();
        StartCoroutine(Fire());
    }
    void Update() {
        SetRandomTime();
    }
    public IEnumerator Fire()
    {
        while(true)
        {
        bulletClone = (Rigidbody) Instantiate(bullet, transform.position, transform.rotation);
        bulletClone.velocity = transform.forward * bulletSpeed;
        yield return new WaitForSeconds(spawnTime);
        }
    }
    void SetRandomTime(){
        spawnTime = Random.Range(minTime, maxTime);
    }
}
