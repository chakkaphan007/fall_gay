﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public string currentState;
    Animator anim;
    const string WALK = "Walk";
    const string IDLE = "Idle";
    // Start is called before the first frame update
    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKey(KeyCode.W)){
            ChangeAnimationState(WALK);
        }
               else if(Input.GetKey(KeyCode.A)){
            ChangeAnimationState(WALK);
        }
                else if(Input.GetKey(KeyCode.S)){
            ChangeAnimationState(WALK);
        }
                else if(Input.GetKey(KeyCode.D)){
            ChangeAnimationState(WALK);
        }
        else{
            ChangeAnimationState(IDLE);
        }
    }
        void ChangeAnimationState(string newState)
    {
        if(currentState == newState) return;
        anim.Play(newState);
        currentState = newState;
    }
}
