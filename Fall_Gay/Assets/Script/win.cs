﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class win : MonoBehaviour
{
    public GameObject Player;
    public GameObject Text;
    void OnTriggerEnter(Collider other) 
    {
        if(other.tag == "Player")
        {
            Text.SetActive(true);
        }
    }
}
