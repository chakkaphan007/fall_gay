﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Des : MonoBehaviour
{
    public GameObject Player;
    public GameObject Spawn;
    void OnTriggerEnter(Collider other) 
    {
        if(other.tag == "Player")
        {
            Player.transform.position = Spawn.transform.position;
        }
            else if(other.tag == "ball")
        {
            Destroy(other);
        }
    }
}
